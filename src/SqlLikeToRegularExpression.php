<?php

/**
 * https://gitlab.com/daffie/sql-like-to-regular-expression.
 *
 * https://www.apache.org/licenses/LICENSE-2.0.txt.
 */

namespace Daffie;

/**
 * Converts SQL LIKE patterns to PHP PCRE (Perl Compatible Regular Expression) patterns.
 */
class SqlLikeToRegularExpression {

  /**
   * Converts an SQL LIKE pattern to a PHP PCRE pattern.
   *
   * The following pattern convertations are to be done:
   *  - All unescaped '_' characters are replaced with an '.' character.
   *  - All unescaped '%' characters are replaced with the '.*' character set.
   *  - All unescaped '^', '$', '.', '[', ']', '{', '}', '(', ')', '?', '*', '+'
   *    character will be converted to escaped characters.
   *  - If the pattern does not start with the '$' character the '^' character
   *    will be added to start of the pattern.
   *  - If the pattern does not end with an unescaped '%' character the '$'
   *    character will be added to the end of the pattern.
   *
   * @param $pattern
   *   The SQL LIKE pattern to be converted.
   *
   * @return string
   *   The to PHP PCRE converted pattern.
   */
  public static function convert($pattern) {
    $last_character_at_start = strlen($pattern) > 0 ? substr($pattern, -1) : '';

    $pattern = self::convertRegularExpressionSpecialCharacters($pattern);

    // If the first character is not "%" then put a "^" at the start of the
    // $pattern.
    if (substr($pattern, 0, 1) != '%') {
      $pattern = '^' . $pattern;
    }

    $pattern = self::convertWildcardCharacters($pattern);

    $last_character_at_end = strlen($pattern) > 1 ? substr($pattern, -2) : '';

    // If the last character of the $pattern was not "%" at the start of the
    // convertion or the last two characters of the pattern are not ".*" at the
    // end of the convertion then add the charater "$" at the end of the pattern.
    if (($last_character_at_start != '%') || ($last_character_at_end != '.*')) {
      $pattern .= '$';
    }

    return $pattern;
  }

  /**
   * Helper method for escaping the regular expressions special characters.
   *
   * @param $pattern
   *   The pattern with unescaped special characters.
   *
   * @return string
   *   The pattern with escaped special characters.
   */
  protected static function convertRegularExpressionSpecialCharacters($pattern) {
    $special_characters = ['^', '$', '.', '[', ']', '{', '}', '(', ')', '?', '*', '+'];
    $special_characters_escaped = ['\^', '\$', '\.', '\[', '\]', '\{', '\}', '\(', '\)', '\?', '\*', '\+'];

    // Replace the "\", "^", "$", ".", "[", "]", "{", "}", "(", ")", "?", "*" and "+" characters.
    $parts = preg_split('/(.*[\^\$\.\[\]\{\}\(\)\?\*\+])/U', $pattern, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
    foreach($parts as &$part) {
      // Test if the last character of the $part is one of the to be escaped
      // characters.
      if (in_array(substr($part, -1), $special_characters, TRUE)) {
        $backslash_count = strlen($part) - strlen(rtrim(substr($part, 0, -1), '\\')) - 1;

        if ($backslash_count >= 0 && !($backslash_count % 2)) {
          // Replace the wildcard only if there are no backslashes or an even
          // number of backslashes. Just replace the unescaped wildcard.
          $part = str_replace($special_characters, $special_characters_escaped, $part);
        }
      }
    }

    // Return the reassemble the $pattern.
    return implode($parts);
  }

  /**
   * Helper method for converting the SQL LIKE wildcard characters to their regular expressions ones.
   *
   * @param $pattern
   *   The pattern with SQL LIKE wildcard characters.
   *
   * @return string
   *   The pattern with regular expression wildcard characters.
   */
  protected static function convertWildcardCharacters($pattern) {
    // Replace the "_" and "%" characters.
    $parts = preg_split('/(.*[_%])/U', $pattern, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
    foreach($parts as &$part) {
      // Test if the last character of the $part is one of the wildcard
      // characters.
      if (in_array(substr($part, -1), ['_', '%'], TRUE)) {
        $backslash_count = strlen($part) - strlen(rtrim(substr($part, 0, -1), '\\')) - 1;

        if ($backslash_count >= 0 && !($backslash_count % 2)) {
          // Replace the wildcard only if there are no backslashes or an even
          // number of backslashes. Just replace the unescaped wildcard.
          $part = str_replace(['_', '%'], ['.', '.*'], $part);
        }
      }
    }

    // Return the reassemble the $pattern.
    return implode($parts);
  }

}
