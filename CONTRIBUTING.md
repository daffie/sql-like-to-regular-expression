## How to contribute

#### Found a bug

Any bug reports are welcomed. Please check if they are not already [reported](https://gitlab.com/daffie/sql-like-to-regular-expression/issues).

#### Write a patch

Pull requests with a patch and clear description are very much welcomed. If applicable add link to corresponding issue.

#### Testing

All new functionality needs testing coverage.

#### Coding conventions

This project follows the Drupal projects [coding standards](https://www.drupal.org/docs/develop/standards).
