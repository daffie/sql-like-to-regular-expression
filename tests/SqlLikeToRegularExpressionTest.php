<?php

/**
 * https://gitlab.com/daffie/sql-like-to-regular-expression.
 *
 * https://www.apache.org/licenses/LICENSE-2.0.txt.
 */

namespace Daffie\Tests;

use Daffie\SqlLikeToRegularExpression;

/**
 * @coversDefaultClass \Daffie\SqlLikeToRegularExpression
 */
class SqlLikeToRegularExpressionTest extends \PHPUnit_Framework_TestCase {

  /**
   * Data provider for testConvert().
   *
   * @return array
   *   An array of arrays containing the following elements:
   *     - The SQL LIKE pattern to be converted.
   *     - The expected regular expression pattern.
   */
  public function providerConvert() {
    return [
      [ 'Hello', '^Hello$'], // Plain text
      [ 'He_lo', '^He.lo$'], // Underscore in middle
      [ 'He%lo', '^He.*lo$'], // Wildcard in middle
      [ 'Hello _', '^Hello .$'], // Trailing underscore
      [ '_ World', '^. World$'], // Leading underscore
      [ 'Hello %', '^Hello .*'], // Trailing wildcard
      [ '% World', '.* World$'], // Leading wildcard
      [ 'He\\_lo', '^He\_lo$'], // Escaped underscore
      [ 'He\\%lo', '^He\%lo$'], // Escaped wildcard
      [ 'He\\\\o', '^He\\\\o$'], // Escaped backslash
      [ 'He\\\\_o', '^He\\\\.o$'], // Backslash and underscore
      [ 'He\\\\%o', '^He\\\\.*o$'], // Backslash and wildcard
      [ 'He\\\\\\_o', '^He\\\\\_o$'], // Backslashx2 and underscore
      [ 'He\\\\\\%o', '^He\\\\\%o$'], // Backslashx2 and wildcard
      [ 'Hello W_%_d', '^Hello W..*.d$'], // Mixed underscore and wildcard
      [ 'Hello W_\\%_d', '^Hello W.\\%.d$'], // Mixed underscore and escaped wildcard
      [ 'H\\l_o', '^H\\l.o$'], // There is an escaped backslash.
      [ 'H\+l_o', '^H\+l.o$'], // There is an escaped "+" character.
      [ 'Hello\%', '^Hello\%$'], // Last character is an escaped "%" character.
      [ 'Hello^World', '^Hello\^World$'], // Escape the "^" character.
      [ 'Hello$World', '^Hello\$World$'], // Escape the "$" character.
      [ 'Hello.World', '^Hello\.World$'], // Escape the "." character.
      [ 'Hello[World', '^Hello\[World$'], // Escape the "[" character.
      [ 'Hello]World', '^Hello\]World$'], // Escape the "]" character.
      [ 'Hello{World', '^Hello\{World$'], // Escape the "{" character.
      [ 'Hello}World', '^Hello\}World$'], // Escape the "}" character.
      [ 'Hello(World', '^Hello\(World$'], // Escape the "(" character.
      [ 'Hello)World', '^Hello\)World$'], // Escape the ")" character.
      [ 'Hello?World', '^Hello\?World$'], // Escape the "?" character.
      [ 'Hello*World', '^Hello\*World$'], // Escape the "*" character.
      [ 'Hello+World', '^Hello\+World$'], // Escape the "+" character.
      [ 'Hello^$.[]{}()?*+World', '^Hello\^\$\.\[\]\{\}\(\)\?\*\+World$'], // Escape all the special characters.
    ];
  }

  /**
   * @covers ::convert
   * @dataProvider providerConvert
   */
  public function testConvert($like_pattern, $expected_pattern) {
    $regex_pattern = SqlLikeToRegularExpression::convert($like_pattern);
    $this->assertEquals($expected_pattern, $regex_pattern, 'The converted pattern is the same as the expected pattern.');
  }

}
